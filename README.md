# Jobs on a Supercomputer with OAR

[TOC]

## Setting up a runner on Grid'5000 / 'Moyen de calcul' frontends

These steps will guide you through installing and configuring a gitlab-runner on Grid'5000 / 'Moyen de calcul' frontends

### Downloading and installing `gitlab-runner` in your home directory

1- Select a Grid'5000 / 'Moyen de Calcul' frontend (here, the Grenoble frontend - aka 'fgrenoble' - is chosen)
2- Run the following commands:

```bash
fgrenoble$ mkdir -p  ~/.local/bin/
fgrenoble$ curl -L --output ~/.local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
fgrenoble$ chmod +x ~/.local/bin/gitlab-runner
```

For more details, check: [GitLab Runner Manual Installation](https://docs.gitlab.com/runner/install/linux-manually.html#install-1)

### Create a basic configuration of gitlab-runner

1- Create the `~/.gitlab-runner` directory:
```bash
fgrenoble$ mkdir ~/.gitlab-runner
```

2- Create `~/.gitlab-runner/config.toml` file and add the following content:
```vim=
check_interval = 0
shutdown_timeout = 0
```

### Running gitlab-runner as a systemd service

#### Activating automatic startup of systemd user instances

```bash
fgrenoble$ loginctl enable-linger
```

#### Creating the service

1- Create the `~/.config/systemd/user/` directory:
```bash
fgrenoble$ mkdir -p  ~/.config/systemd/user/
```
2- Create the `~/.config/systemd/user/gitlab-runner.service` file with the provided content. Replace `<LOGIN>` with your login:

```vim=
[Unit]
Description=GitLab Runner
ConditionFileIsExecutable=/home/<LOGIN>/.local/bin/gitlab-runner
After=syslog.target network.target 

[Service]
StartLimitInterval=5
StartLimitBurst=10
ExecStart=/home/<LOGIN>/.local/bin/gitlab-runner "run" "--working-directory" "/home/<LOGIN>" "--config" "/home/<LOGIN>/.gitlab-runner/config.toml" "--service" "gitlab-runner"
Restart=always
RestartSec=120

[Install]
WantedBy=default.target
```

### Reloading systemd and enabling the service

```bash
fgrenoble$ systemctl --user daemon-reload
fgrenoble$ systemctl --user enable --now gitlab-runner.service
```

### Checking the status of the service

```bash
fgrenoble$ systemctl --user status gitlab-runner.service
```

You can review full logs with `journalctl --user -u gitlab-runner.service`.


### Registering a runner

#### Activating CI/CD in a GitLab project

1. Navigate to the '*Settings*' of a GitLab project (or `https://gitlab.inria.fr/<your_project>/edit#js-shared-permissions`)
2. In the '*Visibility, project features, permissions*' section, enable '*CI/CD*'.

#### Registering a new runner in GitLab

1. From the '*Settings*' menu, choose '*CI/CD*' (or `https://gitlab.inria.fr/<your_project>/-/settings/ci_cd#js-runners-settings`)
2. In the '*Runner*' section, click on '*New project runner*'. Follow the form instructions and click on '*Create runner*'
3. Execute the command provided by GitLab on the frontend (something like `gitlab-runner register --url https://gitlab.inria.fr --token ...`)


## Creating a gitlab-ci.yml

### Setting up a template for reserving nodes

Here is a template that can be expand to perform custom tasks on compute nodes:
- The 'before_script' section includes commands to reserve nodes and wait until they are ready. By default, it reserves a single node for a duration of 10 minutes. You can modifiy these setting by using these variables:
  - 'WALLTIME' determines the reservation duration.
  - 'TYPE' specifies the job type (e.g., besteffort, deploy, etc.).
  - 'NODES' indicates the number of nodes needed.
- The 'after_script' section contains commands to delete the reservation and wait until it is completely finished.


```
.provision-g5k-nodes:
  variables:
    WALLTIME: '0:10'
    TYPE:
    NODES: 1
  before_script:
    - export OAR_JOB_ID="$(oarsub -n $CI_PIPELINE_ID ${TYPE:+-t $TYPE} -l nodes=$NODES,walltime=$WALLTIME 'sleep infinity' | grep OAR_JOB_ID | sed 's/.*=//')"
    - sleep 10
    - while ! oarstat -s -j $OAR_JOB_ID | grep -q Running; do
        oarstat -s -j $OAR_JOB_ID | grep -q Error && exit 1
        echo "Waiting for job $OAR_JOB_ID to be running (expected at $(oarstat -fj $OAR_JOB_ID | grep 'scheduled_start =' | sed 's/.*scheduled_start = //'))...";
        sleep 60;
      done
    - mapfile -t NODES < <(oarstat -j $OAR_JOB_ID -p | oarprint core -P host -f - | sort | uniq)
    - export NODE=${NODES[0]}
    - export FIRST_NODE=${NODES[0]}
    - export LAST_NODE=${NODES[1]}
  after_script:
    # Need to redefined OAR_JOB_ID as after_script script is executed in a different shell
    - export OAR_JOB_ID="$(oarstat -u -J | jq --arg CI_PIPELINE_ID "$CI_PIPELINE_ID" '.[] | select(.name==$CI_PIPELINE_ID) | .id')"
    - oardel $OAR_JOB_ID
    - sleep 15
    - while ! oarstat -s -j $OAR_JOB_ID | grep -q 'Terminated\|Error'; do
        echo "Waiting for job $OAR_JOB_ID to be finished...";
        sleep 60;
      done
    - rm OAR.$OAR_JOB_ID.std*
```

### Using the template in your Gitlab steps

The following dummy example uses the previous template (`extends: .provision-g5k-nodes`). It modifies the default walltime to 4 minutes (`WALLTIME: '0:04'`).
It performs 3 commands on the compute node: `echo $HOSTNAME`, `ls`, and `cat README.md`. You can replace these commands based on your requirements (for instance, compiling a library, running an MPI program, etc.)."

```
build:
  extends: .provision-g5k-nodes
  variables:
    WALLTIME: '0:04'
  stage: build
  tags:
   - g5k
   - grenoble
  script:
   - |
     oarsh $NODE "
        # To fix issue with XDG_RUNTIME_DIR env variable
        # see https://intranet.grid5000.fr/bugzilla/show_bug.cgi?id=15025
        source /etc/profile

        # Makes the server print the executed commands to stdout.
        # Makes the execution stop when one of the commands fails.
        set -x -e

        cd $CI_PROJECT_DIR

        echo $HOSTNAME
        ls
        cat README.md
     "
```
